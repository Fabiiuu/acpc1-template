# ACPC1-Template

Lab Report Template for the ACPC 1 Lab course 

Questions? Criticism? Email me:
[schochal@student.ethz.ch](mailto:schochal@student.ethz.ch)

## Download

Click on the Download Button (to the left of "Clone") to download the template.
You can then upload `template.tex` and `acpc1.cls` to overleaf or use them
locally.

## Local usage

If you choose to work with LaTeX locally, you'll have to install a distribution
and an editor. My suggestions for this are:

- Windows: MikTeX (Distribution), TeXstudio (Editor)
- Mac: MacTeX (Distriution), TeXstudio (Editor)
- Linux: TeXlive (Distribution), Kile or Setzer (Editor)

## How to use the template

First, enter the values the template needs in the preamble. The corresponding
commands are already commented out in `template.tex`.

- `\title{}` sets the title of your report. This is not just the name of the experiment!
- `\authorOne{}{}` sets the first author of your report (name and email).
- `\authorTwo{}{}` sets the second author of your report (name and email).
- `\authorThree{}{}` sets the third author of your report (name and email), if you are a group of three. If you aren't, just don't use this command.
- `\assistant{}` sets the assistant name.
- `\abstract{}` sets the abstract.
- `\place{}` sets the place of signature.
- '\experiment{}` sets the experiment name (e.g. "Dampfdruck") for the page headers.

From the data you enter into those commands, `\maketitle` will generate a title
page and headers.

### Literature

To cite literature (e.g. after comparison of measured results with literature
    values), you can use bibtex or biblatex. You'll find a short introduction
[here](https://www.overleaf.com/learn/latex/Bibliography_management_with_bibtex). 
### Code

To include your code in the appendix, you can use
`\lstinputlisting[caption={Insert Caption}]{your-R-file.R}`. It is already
configured for R.

### Lab journal

After Scanning your Lab journal into a pdf file, you can include it _via_
`\includepdf[pages=-]{your-lab-journal.pdf}`.

### Correctly display results

For this, the LaTeX package `siunitx` is used. This package provides four
commands:

- `\SI{}{}` (for number and unit)
- `\num{}` (only for numbers)
- `\si{}` (only for units)
- `\SIrange{}{}{}` (for a range and a unit)

All those commands work both in normal mode and in math mode.

The *numbers* input (first `{}` in `\SI{}{}`) is just whatever number should be
printed. If you want to display numbers in a scientific manner, you can use the
`e` notation (e.g. `1.2 * 10^5` is `1.2e5`). For confidence intervals, you can
write e.g. `1.25 \pm 0.20` ("pm" stands for "plus-minus"). For a combination of
uncertainty and scientific notation, only write the power of ten after the
conficence interval (e.g. `1.2 \pm 0.3e5`).

The *unit* input (second `{}` in `\SI{}{}`) is the english pronounciation of
said unit. All prefixes and suffixes are separate commands, and so are powers.
A few examples:

- `\milli\meter\per\kelvin`
- `\per\second`
- `\newton\per\square\meter`
- `\joule\per\mol\per\kelvin`

Note that °C is expressed via `\celsius`.

Thus, we can use the command as follows: `$R = \SI{8.314}{\joule\per\mol\per\kelvin}$` or `$\Delta_\text{v}H = \SI{85 \pm 10}{\joule\per\kelvin\per\mol}$`

### Images

Just use those tempates for inserting images.

One image:

```
\begin{figure}[!ht]
  \centering
  \begin{minipage}{.66\linewidth}
    \includegraphics[width=\linewidth]{your-image.pdf}
  \end{minipage}
  \begin{minipage}{.33\linewidth}
    \caption{write image caption here}
    \label{fig:your-image}
  \end{minipage}
\end{figure}
```

Two images:

```
\begin{figure}[!ht]
  \begin{subfigure}{.49\linewidth}
    \includegraphics[width=\linewidth]{your-image-1.pdf}
    \caption{write image caption 1 here}
    \label{fig:your-image-1}
  \end{subfigure}\hfill
  \begin{subfigure}{.49\linewidth}
    \includegraphics[width=\linewidth]{your-image-2.pdf}
    \caption{write image caption 2 here}
    \label{fig:your-image-2}
  \end{subfigure}
  \caption{write overall caption here}
  \label{fig:your-overall-image}
\end{figure}
```
